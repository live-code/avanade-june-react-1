export type Role = 'moderator' | 'admin' | 'guest';
