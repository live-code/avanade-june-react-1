// import axios from 'axios';
import ReactDOM from 'react-dom/client'
// import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import App from './App.tsx'
import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css'
// import { User } from './model/user.ts';
// import DemoCounter from './pages/counter/DemoCounter.tsx';
// import { DemoListHTTP } from './pages/list-http/DemoListHTTP.tsx';
// import { DemoListHTTP2 } from './pages/list-http/DemoListHTTP2.tsx';
// import { DemoList } from './pages/list/DemoList.tsx';
// import { Root } from './Root.tsx';


ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <App />
)


/**
 * Nuova configurazione Router 6.4+
 */
/*
const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    // loader: rootLoader,
    children: [
      {
        path: "counter",
        element: <DemoCounter />,
        // loader: teamLoader,
      },
      {
        path: "list",
        element: <DemoList />,
        // loader: teamLoader,
      },
      {
        path: "users2",
        element: <DemoListHTTP2 />,
        loader: async () => {
           const res =  await axios.get<User[]>('https://jsonplaceholder.typicode.com/users')
          return res.data;
        }
          // loader: teamLoader,
      },
    ],
  },
  {
    path: '*',
    element: <div>page 404</div>
  }
]);

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <RouterProvider router={router} />
)
*/
