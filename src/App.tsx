import { lazy, Suspense } from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { NavBar } from './core/NavBar.tsx';
import { Crud } from './pages/crud/Crud.tsx';
import { DemoControlledForm } from './pages/forms/DemoControlledForm.tsx';
// import DemoCounter from './pages/counter/DemoCounter.tsx';
import { DemoUncontrolledForm } from './pages/forms/DemoUncontrolledForm.tsx';
import { Landing } from './pages/landing/Landing.tsx';
import { DemoList } from './pages/list/DemoList.tsx';
import { DemoListHTTP } from './pages/list-http/DemoListHTTP.tsx';
import { DemoStyling } from './pages/styling/DemoStyling.tsx';
import { UIKitMapsDemo } from './pages/uikit/pages/UIKitMapsDemo.tsx';
import { UIKitPanelsDemo } from './pages/uikit/pages/UIKitPanelsDemo.tsx';
import { UIKitTabbarDemo } from './pages/uikit/pages/UIKitTabbarDemo.tsx';
import { UiKitDemo } from './pages/uikit/UiKitDemo.tsx';
import { UsersDetails } from './pages/user-details/UsersDetails.tsx';

const DemoCounter = lazy(() => import('./pages/counter/DemoCounter'))

function App() {

  return (
    <BrowserRouter>
      <NavBar />

      <Routes>
        <Route path="counter" element={
          <Suspense fallback={<div>loading...</div>}>
            <DemoCounter />
          </Suspense>
        } />
        <Route path="list" element={<DemoList />}  />
        <Route path="users" element={<DemoListHTTP />} />
        <Route path="users/:userId" element={<UsersDetails />} />
        <Route path="styling" element={<DemoStyling />} />

        <Route
          path="uikit"
          element={<UiKitDemo />}
        >
          <Route path="panels" element={<UIKitPanelsDemo />} />
          <Route path="maps" element={<UIKitMapsDemo />} />
          <Route path="tabbar" element={<UIKitTabbarDemo />} />
          <Route index element={
            <Navigate to="panels" />
          } />
        </Route>

        <Route path="landing" element={<Landing />} />
        <Route path="crud" element={<Crud />} />
        <Route path="forms-uncontrolled" element={<DemoUncontrolledForm />} />
        <Route path="forms-controlled" element={<DemoControlledForm />} />
        <Route path="/" element={ <div>Welcome page</div>} />
        <Route path="*" element={
          <Navigate to="/" />
        }/>
      </Routes>


    </BrowserRouter>
  )
}

export default App;

