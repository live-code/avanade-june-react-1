import { NavLink } from 'react-router-dom';

const isActive = (obj: { isActive: boolean }) => {
  return obj.isActive ? 'btn btn-dark' : 'btn btn-primary'
}

export function NavBar() {
  // const location = useLocation();
  // console.log(location)
  return (
    <>
    <div className="btn-group">
      <NavLink to="counter" className={isActive}>Counter</NavLink>
      <NavLink to="list"  className={isActive}>List</NavLink>
      <NavLink to="users" className="btn btn-primary">Http</NavLink>
      <NavLink to="styling" className="btn btn-primary">Styling</NavLink>
      <NavLink to="uikit" className="btn btn-primary">UIKIT</NavLink>
      <NavLink to="landing" className="btn btn-primary">landing</NavLink>
      <NavLink to="forms-uncontrolled" className="btn btn-primary">forms 1</NavLink>
      <NavLink to="forms-controlled" className="btn btn-primary">forms 2</NavLink>
      <NavLink to="crud" className="btn btn-primary">crud</NavLink>
    </div>
    <hr/>
  </>
  )
}
