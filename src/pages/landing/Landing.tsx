import { Compare } from './components/Compare.tsx';
import { Footer } from './components/Footer.tsx';
import { NavBar } from './components/NavBar.tsx';
import { Pricing } from './components/Pricing.tsx';

export function Landing() {
  return <>
    <div className="container py-3">
      <NavBar />
      <main>
        <Pricing />
        <Compare />
      </main>
      <Footer />
    </div>
  </>

}
