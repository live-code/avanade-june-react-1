import { useState } from 'react';
import { PageTitle } from '../../shared/PageTitle.tsx';
import { ToJson } from '../../shared/ToJson.tsx';

const initialState = [
  { id: 11, name: 'Fabio' },
  { id: 22, name: 'Lorenzo' },
  { id: 33, name: 'Silvia' },
];


export function DemoList() {
  const [list, setList] = useState(initialState)

  function addUser() {
    setList(
      [...list, { id: Date.now(), name: 'xyz'}]
    )
  }

  function deleteUser(idToRemove: number) {
    setList(
      list.filter(item => item.id !== idToRemove)
    )
  }

  return <div>
    <PageTitle>Demo List</PageTitle>

    <button onClick={addUser}>Add User</button>

    {
      list.map((item) => {
        return (
          <li key={item.id}>
             {item.name}
            <button onClick={() => deleteUser(item.id)}>Delete</button>
          </li>
        )
      })
    }


    <ToJson data={list} />
  </div>
}


