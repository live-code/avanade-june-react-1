import { Card } from '../../../shared/Card.tsx';
import { Panel } from '../../../shared/Panel.tsx';

export function UIKitPanelsDemo() {

  function openGoogle() {
    window.open('https://www.google.com')
  }

  function doSomething() {
    console.log('log!!!!')
  }

  return <div>
    <h1>Uikit demo</h1>

    <Card
      title="card 1"
      icon="fa fa-google"
      iconClick={openGoogle}
      variant="success"
    >
      lorem ipsum
    </Card>

    <Card
      title="card 2"
      icon="fa fa-facebook"
      iconClick={doSomething}
    >
      lorem ipsum
    </Card>


    <br/>

    <Panel>
      <Panel.Title>WIDGET TITLE</Panel.Title>
      <Panel.Body>lorem</Panel.Body>
      <Panel.Body>lorem</Panel.Body>
      <Panel.Body>lorem</Panel.Body>
    </Panel>
  </div>
}
