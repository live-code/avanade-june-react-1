
interface ProductsProps {
  value: number;
}

export function Products(props: ProductsProps) {
  return (
    <div>
      <h1>there are {props.value} products</h1>
    </div>
  )
}
