import NoItems from './components/NoItems.tsx';
import { Products } from './components/Products.tsx';
import { useCounter } from './hooks/useCounter.ts';

function DemoCounter() {
  const { value, increment, decrement } = useCounter();

  return (
    <>
      <div>Demo Counter</div>
      <button onClick={() => decrement(1)}>-</button>
      <button onClick={() => increment(2)}>+</button>

      <div>
        {
          value ?
            <Products value={value} /> :
            <NoItems />
        }
      </div>

      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.rferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.rferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.rferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis eaque enim fugit mollitia perferendis. Alias atque ea eius esse maiores nihil nisi odit, omnis perferendis quia quis totam voluptatem voluptatum.

    </>
  )
}

export default DemoCounter;
